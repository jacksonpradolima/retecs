import agents
import datetime
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import multiprocessing
import numpy as np
import os
import pandas as pd
import retecs
import reward
import scenarios
import seaborn as sns
import stats
import time

from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import MultipleLocator

USE_LATEX = False


# if USE_LATEX:
#     mpl.use('pgf')
# else:
#     mpl.use('Agg')


def figsize_column(scale, height_ratio=1.0):
    fig_width_pt = 240  # Get this from LaTeX using \the\textwidth
    inches_per_pt = 1.0 / 72.27  # Convert pt to inch
    # Aesthetic ratio (you could change this)
    golden_mean = (np.sqrt(5.0) - 1.0) / 2.0
    fig_width = fig_width_pt * inches_per_pt * scale  # width in inches
    fig_height = fig_width * golden_mean * height_ratio  # height in inches
    fig_size = [fig_width, fig_height]
    return fig_size


def figsize_text(scale, height_ratio=1.0):
    fig_width_pt = 504  # Get this from LaTeX using \the\textwidth
    inches_per_pt = 1.0 / 72.27  # Convert pt to inch
    # Aesthetic ratio (you could change this)
    golden_mean = (np.sqrt(5.0) - 1.0) / 2.0
    fig_width = fig_width_pt * inches_per_pt * scale  # width in inches
    fig_height = fig_width * golden_mean * height_ratio  # height in inches
    fig_size = [fig_width, fig_height]
    return fig_size


pgf_with_latex = {  # setup matplotlib to use latex for output
    "pgf.texsystem": "pdflatex",  # change this if using xetex or lautex
    "text.usetex": True,  # use LaTeX to write all text
    "font.family": "serif",
    "font.serif": [],  # blank entries should cause plots to inherit fonts from the document
    "font.sans-serif": [],
    "font.monospace": [],
    "axes.labelsize": 9,
    "font.size": 9,
    "legend.fontsize": 7,
    "xtick.labelsize": 7,
    "ytick.labelsize": 7,
    "figure.figsize": figsize_column(1.0),  # default fig size of 0.9 textwidth
    "pgf.preamble": [
        # use utf8 fonts becasue your computer can handle it :)
        r"\usepackage[utf8x]{inputenc}",
        # plots will be generated using this preamble
        r"\usepackage[T1]{fontenc}",
    ]
}


if USE_LATEX:
    sns.set_style('whitegrid', pgf_with_latex)
else:
    sns.set_style('whitegrid')

sns.set_context('paper')
sns.set_palette(sns.color_palette("Set1", n_colors=8, desat=.5))


ITERATIONS = 30
CI_CYCLES = 1000

PARALLEL = True

RUN_EXPERIMENT = True
VISUALIZE_RESULTS = True

method_names = {
    'mlpclassifier': 'Network',
    'tableau': 'Tableau',
    'heur_random': 'Random',
    'heur_sort': 'Sorting',
    'heur_weight': 'Weighting'
}

reward_names = {
    'failcount': 'Failure Count Reward',
    'tcfail': 'Test Case Failure Reward',
    'timerank': 'Time-ranked Reward'
}

env_names = {
    'alibaba@druid': 'Druid',
    'alibaba@fastjson': 'Fastjson',
    'deeplearning4j@deeplearning4j': 'Deeplearning4j',
    'DSpace@DSpace': 'DSpace',
    'google@guava': 'Guava',
    'square@okhttp': 'OkHttp',
    'square@retrofit': 'Retrofit',
    'zxing@zxing': 'ZXing',
    'lexisnexis': 'LexisNexis',
    'iofrol': 'ABB IOF/ROL',
    'paintcontrol': 'ABB Paint Control',
    'gsdtsr': 'GSDTSR',
}

PARALLEL_POOL_SIZE = 1

# With 93GB RAM try to use:
# 15 threads
#DATASETS_EXPERIMENTS = ['deeplearning4j@deeplearning4j', 'alibaba@druid', 'square@retrofit',
#                        'zxing@zxing', 'lexisnexis', 'paintcontrol', 'iofrol']

# 10 threads
#DATASETS_EXPERIMENTS = ['square@okhttp', 'gsdtsr']

# 5 threads
#DATASETS_EXPERIMENTS = ['google@guava', 'DSpace@DSpace']

# 1 thread
DATASETS_EXPERIMENTS = ['alibaba@fastjson']

#SCHED_TIME_RATIO = 0.5
#SCHED_TIME_RATIO = 0.8
SCHED_TIME_RATIO = 0.1

DATA_DIR = 'RESULTS'
FIGURE_DIR = 'RESULTS'

DATA_DIR = os.path.join(DATA_DIR, 'time_ratio_%s' % int(SCHED_TIME_RATIO*100))
FIGURE_DIR = os.path.join(FIGURE_DIR, 'time_ratio_%s' %
                          (SCHED_TIME_RATIO*100))

if not os.path.exists(DATA_DIR):
    os.mkdir(DATA_DIR)


def get_scenario(name):
    if name == 'incremental':
        sc = scenarios.IncrementalScenarioProvider(episode_length=CI_CYCLES)
    else:
        sc = scenarios.IndustrialDatasetScenarioProvider(
            tcfile='DATA/'+name+'.csv', sched_time_ratio=SCHED_TIME_RATIO)
    return sc


def run_experiments(exp_fun, parallel=PARALLEL):
    # NOTE: to find problems in the code run only this statement
    # avg_res = exp_run_industrial_datasets(1)

    # Compute time
    start = time.time()

    if parallel:
        p = multiprocessing.Pool(PARALLEL_POOL_SIZE)
        avg_res = p.map(exp_fun, [(i + 1) for i in range(ITERATIONS)])
    else:
        avg_res = [exp_fun(i) for i in range(ITERATIONS)]

    end = time.time()

    print('Ran experiments: %d results' % len(avg_res))
    print('Time expend to run the experiments: %.2f' % (end-start))


def exp_run_industrial_datasets(iteration, datasets=DATASETS_EXPERIMENTS):
    ags = [
        lambda: (
            agents.TableauAgent(histlen=retecs.DEFAULT_HISTORY_LENGTH, learning_rate=retecs.DEFAULT_LEARNING_RATE,
                                state_size=retecs.DEFAULT_STATE_SIZE,
                                action_size=retecs.DEFAULT_NO_ACTIONS, epsilon=retecs.DEFAULT_EPSILON),
            retecs.preprocess_discrete, reward.timerank),
        lambda: (agents.NetworkAgent(histlen=retecs.DEFAULT_HISTORY_LENGTH, state_size=retecs.DEFAULT_STATE_SIZE,
                                     action_size=1,
                                     hidden_size=retecs.DEFAULT_NO_HIDDEN_NODES), retecs.preprocess_continuous,
                 reward.tcfail)
    ]

    reward_funs = {
        # 'failcount': reward.failcount,
        'timerank': reward.timerank,
        'tcfail': reward.tcfail
    }

    avg_napfd = []

    for i, get_agent in enumerate(ags):
        for sc in datasets:
            for (reward_name, reward_fun) in reward_funs.items():
                agent, preprocessor, _ = get_agent()
                file_appendix = 'rq_%s_%s_%s_%d' % (
                    agent.name, sc, reward_name, iteration)

                scenario = get_scenario(sc)

                rl_learning = retecs.PrioLearning(agent=agent,
                                                  scenario_provider=scenario,
                                                  reward_function=reward_fun,
                                                  reward_name=reward_name,
                                                  preprocess_function=preprocessor,
                                                  file_prefix=file_appendix,
                                                  dump_interval=100,
                                                  validation_interval=0,
                                                  iteration=iteration,
                                                  output_dir=DATA_DIR)
                res = rl_learning.train(no_scenarios=CI_CYCLES,
                                        print_log=True,
                                        plot_graphs=False,
                                        save_graphs=False,
                                        collect_comparison=False)
                avg_napfd.append(res)

    return avg_napfd


def save_figures(fig, filename):
    if USE_LATEX:
        fig.savefig(os.path.join(FIGURE_DIR, filename + '.pgf'),
                    bbox_inches='tight')

    fig.savefig(os.path.join(FIGURE_DIR, filename + '.pdf'),
                bbox_inches='tight')
